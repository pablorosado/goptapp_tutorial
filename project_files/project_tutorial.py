# Project tutorial: dummy project to show how optapp/goptapp work.

# Define required paths.
par_INFOFILES='../infofiles/'
par_APPROACHES='../code/approaches/'
par_SUBDATASETS='../subdatasets/'

# Define other Optapp options.
par_n_splits = 2
par_labels_col = "label"
par_parlist_plot = ["approach", "app_0000_example_float"]


