# Define each parameter starting with 'app_XXXX' (e.g. app_0003_example_par = ...).
# The values of each parameter must be given within a list.
#    If parameter value is fixed, that list should contain one element (the fixed value).
#    If parameter value ranges from 'low' to 'high', and is integer, the list should contain [low, high, True].
#    If parameter value ranges from 'low' to 'high', and is float, the list should contain [low, high, Float].

app_0000_example_float = [0.1, 0.5, False]
app_0000_example_int = [0, 3, True]
app_0000_example_fixed = [4]
