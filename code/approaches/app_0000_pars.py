# Define each parameter starting with 'app_XXXX' (e.g. app_0003_example_par = ...).
# The possible values of each parameter must be given within a list (with at least one element).

app_0000_example_float = [0.1, 0.2, 0.3, 0.5]
app_0000_example_int = [0, 1, 2, 3]
app_0000_example_fixed = [4]
