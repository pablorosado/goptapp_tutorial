########################################################################################################################

description='Example approach for tutorial.'

########################################################################################################################

# Import modules
import time, os, sys
import numpy as np
import pandas as pd
import optapp.functions as fun
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import precision_score, recall_score, f1_score

########################################################################################################################

# Define fixed inputs.


########################################################################################################################

# Main function: it must have an input 'run' and return a dictionary of results.
#def main(run):
#    ...
#    return results
#
# The dictionary of results must contain at least the following keys:
#   results = {}
#   results['score_...'] = list containing main score (one number, or as many as labels, if there are labels) [required]
#   results['time_...'] = list containing main execution time [required]
#   return results
#
# If data has labels, the results dictionary must also contain:
#   results['labels'] = list of labels [required if data has labels, otherwise this key should not exist]
#
# Optionally, more data can be stored in results dictionary, like other scores ('score_...') or times ('time_...'):
#   results['score_...'] = list containing other score (e.g. training set score) [not required]
#   results['time_...'] = list containing some other execution time [not required]
#   results['other_data'] = list containing some other data [not required]


def main(run):

    # Get parameter values for this run.
    pars = fun.get_pars(run)

    # Get train and test (or dev) indexes for this run.
    train_inds, test_inds = fun.get_train_and_test_indexes(run)

    # Start timer.
    tic = time.time()

    # Load data.
    dataset_dir = os.path.join(pars.par_PROJECTFILES,'..','datasets','dataset.csv')
    data = pd.read_csv(dataset_dir, index_col='id')
    features = data[['feature1', 'feature2']]
    labels = data.label

    # Get train and test (or dev) data.
    train_X = features.loc[train_inds].values
    train_y = labels.loc[train_inds].values
    test_X = features.loc[test_inds].values

    # Define model.
    model = LogisticRegression(solver='lbfgs',
                               random_state=pars.par_random_seed,
                               C=pars.app_0000_example_float,
                               multi_class='auto')

    # Train algorithm.
    model.fit(train_X, train_y)

    # Predict on test set.
    pred = model.predict(test_X)

    # Evaluate model.
    true = labels.loc[test_inds].values
    all_labels = np.unique(true)
    precision = precision_score(true, pred, average=None, labels=all_labels)
    recall = recall_score(true, pred, average=None, labels=all_labels)
    f1 = f1_score(true, pred, average=None, labels=all_labels)

    # Stop timer.
    toc = time.time()
    lapse = toc-tic

    # Create dictionary of output results.
    results = {'score_f1': f1.tolist(),
               'score_precision': precision.tolist(),
               'score_recall': recall.tolist(),
               'time_all': [lapse],
               'labels': all_labels.tolist()}

    return results

########################################################################################################################
