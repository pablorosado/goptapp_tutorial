# Template project for OptApp/Goptapp. 

## Example use:

### Import goptapp.

Go to folder 'tutorial/project_files/' and start a jupyter notebook from there (in other words, the working directory must be 'tutorial/project_files/' so that all paths work).

In the notebook, import goptapp:

	import goptapp


### Start a pipeline.

Now start a graphical pipeline:

	goptapp.gui.start();

By default, goptapp will assume that the path to your project files is the current working directory.

Click on 'start pipeline'.

### Load a project.

Go to 'Manage projects' and 'Select project'. Project 'tutorial' should already be selected (since there are no other projects).

### Create and load a subset.

Go to 'Manage subsets' and 'Select subset'. There you should see no subsets (they do not exist yet!).

Go to 'Create new subset' and in the 'Parameters' tab, click on 'Go!'. Then go to the 'Ouputs' tab and check if a new subset has been created. If so, the message should say:
"Generating subdataset ../subdatasets//0000/."

Now you can go back to 'Select subset' and choose subset '0000' from the dropdown menu.

### Execute an approach.

Go to 'Manage runs' and 'Automatic optimisation', and click on 'Go!'. Then go to the 'Outputs' tab and check if runs are being executed.

You can also try 'Manual optimisation' in an analogous way.

Then go to 'Compare runs' and click on 'Go!'. In 'Outputs' tab you can see the comparison of the different runs.

### Choose the best approach.

Go to 'Manage results' and 'Choose best approach', and click on 'Go!'. Then go to the 'Outputs' tab and check the output (there was only one approach, so the choice is trivial).

### Final evaluation.

TODO: Not yet implemented in goptapp (only in optapp).


